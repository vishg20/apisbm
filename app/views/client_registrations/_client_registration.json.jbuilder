json.extract! client_registration, :id, :company_name, :buyer_name, :phone_number, :email_id, :mobile_number, :purchasing_manager_name, :address, :nature_of_business, :login_id, :tenant_id, :client_registration_id, :created_at, :updated_at
json.url client_registration_url(client_registration, format: :json)
