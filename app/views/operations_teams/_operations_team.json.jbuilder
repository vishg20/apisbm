json.extract! operations_team, :id, :name, :mobile_number, :email_id, :login_id, :tenant_id, :created_at, :updated_at
json.url operations_team_url(operations_team, format: :json)
