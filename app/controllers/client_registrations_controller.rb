class ClientRegistrationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_client_registration, only: [:show, :update, :destroy]

  # GET /client_registrations
  # GET /client_registrations.json
  def index
    @client_registrations = ClientRegistration.all
  end

  # GET /client_registrations/1
  # GET /client_registrations/1.json
  def show
  end
   # GET /client_registrations/new
  def new
    @client_registration = ClientRegistration.new
   
  end
  # POST /client_registrations
  # POST /client_registrations.json
  def create
    @client_registration = ClientRegistration.new(client_registration_params)

    if @client_registration.save
      render :show, status: :created, location: @client_registration
    else
      render json: @client_registration.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /client_registrations/1
  # PATCH/PUT /client_registrations/1.json
  def update
    if @client_registration.update(client_registration_params)
      render :show, status: :ok, location: @client_registration
    else
      render json: @client_registration.errors, status: :unprocessable_entity
    end
  end

  # DELETE /client_registrations/1
  # DELETE /client_registrations/1.json
  def destroy
    @client_registration.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_registration
      @client_registration = ClientRegistration.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_registration_params
      params.require(:client_registration).permit(:company_name, :buyer_name, :phone_number, :email_id, :mobile_number, :purchasing_manager_name, :address, :nature_of_business, :login_id, :tenant_id, :client_registration_id)
    end
end
