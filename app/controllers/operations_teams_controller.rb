class OperationsTeamsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_operations_team, only: [:show, :update, :destroy]

  # GET /operations_teams
  # GET /operations_teams.json
  def index
    @operations_teams = OperationsTeam.all
  end

  # GET /operations_teams/1
  # GET /operations_teams/1.json
  def show
  end
    # GET /operations_teams/new
  def new
    @operations_team = OperationsTeam.new
     
  end
  # POST /operations_teams
  # POST /operations_teams.json
  def create
    @operations_team = OperationsTeam.new(operations_team_params)

    if @operations_team.save
      render :show, status: :created, location: @operations_team
    else
      render json: @operations_team.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /operations_teams/1
  # PATCH/PUT /operations_teams/1.json
  def update
    if @operations_team.update(operations_team_params)
      render :show, status: :ok, location: @operations_team
    else
      render json: @operations_team.errors, status: :unprocessable_entity
    end
  end

  # DELETE /operations_teams/1
  # DELETE /operations_teams/1.json
  def destroy
    @operations_team.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_operations_team
      @operations_team = OperationsTeam.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def operations_team_params
      params.require(:operations_team).permit(:name, :mobile_number, :email_id, :login_id, :tenant_id)
    end
end
