class EnquiryDetailsPagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_enquiry_details_page, only: [:show, :update, :destroy]

  # GET /enquiry_details_pages
  # GET /enquiry_details_pages.json
  def index
    @enquiry_details_pages = EnquiryDetailsPage.all
  end

  # GET /enquiry_details_pages/1
  # GET /enquiry_details_pages/1.json
  def show
  end
  # GET /enquiry_details_pages/new
  def new
    @enquiry_details_page = EnquiryDetailsPage.new  
  
  end

  # POST /enquiry_details_pages
  # POST /enquiry_details_pages.json
  def create
    @enquiry_details_page = EnquiryDetailsPage.new(enquiry_details_page_params)

    if @enquiry_details_page.save
      render :show, status: :created, location: @enquiry_details_page
    else
      render json: @enquiry_details_page.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /enquiry_details_pages/1
  # PATCH/PUT /enquiry_details_pages/1.json
  def update
    if @enquiry_details_page.update(enquiry_details_page_params)
      render :show, status: :ok, location: @enquiry_details_page
    else
      render json: @enquiry_details_page.errors, status: :unprocessable_entity
    end
  end

  # DELETE /enquiry_details_pages/1
  # DELETE /enquiry_details_pages/1.json
  def destroy
    @enquiry_details_page.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enquiry_details_page
      @enquiry_details_page = EnquiryDetailsPage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enquiry_details_page_params
      params.require(:enquiry_details_page).permit(:enquiry_number, :enquiry_details, :quotes_and_delivery_schedules, :tenant_id)
    end
end
