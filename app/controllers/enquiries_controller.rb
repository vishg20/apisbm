class EnquiriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_enquiry, only: [:show, :update, :destroy]

  # GET /enquiries
  # GET /enquiries.json
  def index
    @enquiries = Enquiry.all
  end

  # GET /enquiries/1
  # GET /enquiries/1.json
  def show
  end
   # GET /enquiries/new
  def new

    @enquiry = Enquiry.new
  
  end
  # POST /enquiries
  # POST /enquiries.json
  def create
    @enquiry = Enquiry.new(enquiry_params)

    if @enquiry.save
      render :show, status: :created, location: @enquiry
    else
      render json: @enquiry.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /enquiries/1
  # PATCH/PUT /enquiries/1.json
  def update
    if @enquiry.update(enquiry_params)
      render :show, status: :ok, location: @enquiry
    else
      render json: @enquiry.errors, status: :unprocessable_entity
    end
  end

  # DELETE /enquiries/1
  # DELETE /enquiries/1.json
  def destroy
    @enquiry.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enquiry
      @enquiry = Enquiry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enquiry_params
      params.require(:enquiry).permit(:client_name, :enquiry_for, :color, :material, :gsm, :quantity, :sizes, :printing, :embroidery, :place_of_printing, :payment_terms, :client_target_price, :client_target_delivery_schedule, :owner_of_the_etask, :status_of_the_enquiry, :tenant_id, :client_id)
    end
end
