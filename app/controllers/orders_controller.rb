class OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_order, only: [:show, :update, :destroy]

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end
  # GET /orders/new
  def new
    @order = Order.new

  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)

    if @order.save
      render :show, status: :created, location: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    if @order.update(order_params)
      render :show, status: :ok, location: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:customer_name, :enquiry_number, :client_name, :enquiry_for, :color, :material, :gsm, :quantity, :sizes, :printing, :embroidery, :place_of_printing, :payment_terms, :client_target_price, :client_target_delivery_schedule, :offered_price, :offered_delivery_schedule, :status_of_the_enquiry, :supplier_name, :sample_readiness, :sample__delivery, :advance_received, :status_of_the_order, :owner_of_the_otask, :tenant_id)
    end
end
