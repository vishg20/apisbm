require 'test_helper'

class EnquiriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @enquiry = enquiries(:one)
  end

  test "should get index" do
    get enquiries_url, as: :json
    assert_response :success
  end

  test "should create enquiry" do
    assert_difference('Enquiry.count') do
      post enquiries_url, params: { enquiry: { client_id: @enquiry.client_id, client_name: @enquiry.client_name, client_target_delivery_schedule: @enquiry.client_target_delivery_schedule, client_target_price: @enquiry.client_target_price, color: @enquiry.color, embroidery: @enquiry.embroidery, enquiry_for: @enquiry.enquiry_for, gsm: @enquiry.gsm, material: @enquiry.material, owner_of_the_etask: @enquiry.owner_of_the_etask, payment_terms: @enquiry.payment_terms, place_of_printing: @enquiry.place_of_printing, printing: @enquiry.printing, quantity: @enquiry.quantity, sizes: @enquiry.sizes, status_of_the_enquiry: @enquiry.status_of_the_enquiry, tenant_id: @enquiry.tenant_id } }, as: :json
    end

    assert_response 201
  end

  test "should show enquiry" do
    get enquiry_url(@enquiry), as: :json
    assert_response :success
  end

  test "should update enquiry" do
    patch enquiry_url(@enquiry), params: { enquiry: { client_id: @enquiry.client_id, client_name: @enquiry.client_name, client_target_delivery_schedule: @enquiry.client_target_delivery_schedule, client_target_price: @enquiry.client_target_price, color: @enquiry.color, embroidery: @enquiry.embroidery, enquiry_for: @enquiry.enquiry_for, gsm: @enquiry.gsm, material: @enquiry.material, owner_of_the_etask: @enquiry.owner_of_the_etask, payment_terms: @enquiry.payment_terms, place_of_printing: @enquiry.place_of_printing, printing: @enquiry.printing, quantity: @enquiry.quantity, sizes: @enquiry.sizes, status_of_the_enquiry: @enquiry.status_of_the_enquiry, tenant_id: @enquiry.tenant_id } }, as: :json
    assert_response 200
  end

  test "should destroy enquiry" do
    assert_difference('Enquiry.count', -1) do
      delete enquiry_url(@enquiry), as: :json
    end

    assert_response 204
  end
end
