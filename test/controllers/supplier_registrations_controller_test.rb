require 'test_helper'

class SupplierRegistrationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @supplier_registration = supplier_registrations(:one)
  end

  test "should get index" do
    get supplier_registrations_url, as: :json
    assert_response :success
  end

  test "should create supplier_registration" do
    assert_difference('SupplierRegistration.count') do
      post supplier_registrations_url, params: { supplier_registration: { address: @supplier_registration.address, company_name: @supplier_registration.company_name, email_id: @supplier_registration.email_id, login_id: @supplier_registration.login_id, manager_name: @supplier_registration.manager_name, mobile_number: @supplier_registration.mobile_number, nature_of_business: @supplier_registration.nature_of_business, phone_number: @supplier_registration.phone_number, supplier_name: @supplier_registration.supplier_name, tenant_id: @supplier_registration.tenant_id } }, as: :json
    end

    assert_response 201
  end

  test "should show supplier_registration" do
    get supplier_registration_url(@supplier_registration), as: :json
    assert_response :success
  end

  test "should update supplier_registration" do
    patch supplier_registration_url(@supplier_registration), params: { supplier_registration: { address: @supplier_registration.address, company_name: @supplier_registration.company_name, email_id: @supplier_registration.email_id, login_id: @supplier_registration.login_id, manager_name: @supplier_registration.manager_name, mobile_number: @supplier_registration.mobile_number, nature_of_business: @supplier_registration.nature_of_business, phone_number: @supplier_registration.phone_number, supplier_name: @supplier_registration.supplier_name, tenant_id: @supplier_registration.tenant_id } }, as: :json
    assert_response 200
  end

  test "should destroy supplier_registration" do
    assert_difference('SupplierRegistration.count', -1) do
      delete supplier_registration_url(@supplier_registration), as: :json
    end

    assert_response 204
  end
end
