require 'test_helper'

class CustomisationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @customisation = customisations(:one)
  end

  test "should get index" do
    get customisations_url, as: :json
    assert_response :success
  end

  test "should create customisation" do
    assert_difference('Customisation.count') do
      post customisations_url, params: { customisation: { place_of_embroidery: @customisation.place_of_embroidery, place_of_printing: @customisation.place_of_printing, tenant_id: @customisation.tenant_id, type: @customisation.type } }, as: :json
    end

    assert_response 201
  end

  test "should show customisation" do
    get customisation_url(@customisation), as: :json
    assert_response :success
  end

  test "should update customisation" do
    patch customisation_url(@customisation), params: { customisation: { place_of_embroidery: @customisation.place_of_embroidery, place_of_printing: @customisation.place_of_printing, tenant_id: @customisation.tenant_id, type: @customisation.type } }, as: :json
    assert_response 200
  end

  test "should destroy customisation" do
    assert_difference('Customisation.count', -1) do
      delete customisation_url(@customisation), as: :json
    end

    assert_response 204
  end
end
