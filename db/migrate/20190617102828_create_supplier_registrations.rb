class CreateSupplierRegistrations < ActiveRecord::Migration[5.2]
  def change
    create_table :supplier_registrations do |t|
      t.string :company_name
      t.string :supplier_name
      t.string :phone_number
      t.string :email_id
      t.string :mobile_number
      t.string :manager_name
      t.string :address
      t.string :nature_of_business
      t.string :login_id
      t.string :tenant_id

      t.timestamps
    end
  end
end
