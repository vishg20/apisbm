class CreateEnquiryDetailsPages < ActiveRecord::Migration[5.2]
  def change
    create_table :enquiry_details_pages do |t|
      t.string :enquiry_number
      t.string :enquiry_details
      t.string :quotes_and_delivery_schedules
      t.string :tenant_id

      t.timestamps
    end
  end
end
